from setuptools import setup, find_packages

with open("requirements.txt", "r", encoding="utf-8") as fh:
    requirements = fh.read()

setup(
    name = 'knd',
    version = '0.0.1',
    author = 'Ram Kannan S',
    author_email = 'ramkannan2612@gmail.com',
    description = 'Kubernetes NGINX deployer',
    packages = find_packages(),
    install_requires = [requirements],
    python_requires='>=3.7',
    classifiers=[
        "Programming Language :: Python :: 3.8",
        "Operating System :: OS Independent",
    ],
    entry_points = '''
        [console_scripts]
        knd=knd_tool:main
    '''
)