import click
import argparse
import time
import progressbar
import os
import git
import sys
import shutil
import yaml
from progressbar import ProgressBar
from shutil import copy

def job(replica,version,deployment):
    widgets = [' [',
         progressbar.Timer(format= 'elapsed time: %(elapsed)s'),
         '] ',
           progressbar.Bar('*'),' (',
           progressbar.ETA(), ') ',
          ]
  
    pbar = progressbar.ProgressBar(max_value=100, widgets=widgets).start()
    total_steps = 4
    # step 1
    checkoutscm()
    pbar.update((1/4)*100) 
    # step 2
    updatevalues(replica,version,deployment)
    pbar.update((2/4)*100) 
    # step 3
    deploynginx()
    pbar.update((3/4)*100) 
    # step 4
    completednotification()
    pbar.update((4/4)*100) 
    pbar.finish()

def checkoutscm():
    print(" Cloning Repo")
    folder_path = "python"
    try:
        shutil.rmtree(folder_path)
    except OSError as e:
        print("Error: %s - %s." % (e.filename, e.strerror))
    git.Git("").clone("https://github.com/kubernetes-client/python.git", recursive=True)

def updatevalues(replica,version,deployment):
    print(" Updating values in Nginx Yaml File")
    time.sleep(5)
    print(f"Replicas = {replica}")
    print(f"Deployment Name = {deployment}")
    print(f"Version = {version}")

    yaml_file = open("python/examples/nginx-deployment.yaml", 'r')
    yaml_content = yaml.load(yaml_file)

    for key, value in yaml_content.items():
        yaml_content['spec']['replicas'] = replica
        yaml_content['spec']['template']['spec']['containers'][0]["name"]  = deployment
        yaml_content['spec']['template']['spec']['containers'][0]["image"]  = 'nginx:' + str(version)

    with open('python/examples/nginx-deployment.yaml', 'w') as f:
        yaml.dump(yaml_content, f, sort_keys=False)

def deploynginx():
    print(" Deploy Nginx..")
    time.sleep(5)
    path = "python/"
  #  copy('test_pods_list.py', 'python/examples/')
    os.chdir(path)
    os.system("python3 setup.py install")
    os.system("pip3 install kubernetes")
  #  os.system("python3 -m examples.test_pods_list")    ## sample to list pods with IP's
    os.system("python3 -m examples.deployment_create")    ## ngnix deployment

def completednotification():
    time.sleep(5)
    print(" Nginx Deployment Completed")

def main():
    parser = argparse.ArgumentParser(description = "Kubernetes NGINX Deployment Manager Tool")

    parser.add_argument("-r", "--replicas", type = int,
                        metavar = "replicas", default = None, required=True, 
                        help = "Specify Number of Replicas")
      
    parser.add_argument("-v", "--nginx-version", 
                        metavar = "nginxversion", default = None, required=True, 
                        help = "Specify the nginx version.")
      
    parser.add_argument("-d", "--deployment-name", 
                        metavar = "deployment-name", default = None, required=True, 
                        help = "Deployment Name for nginx.")
  
    args = parser.parse_args()
    print(args)

    job(args.replicas,args.nginx_version,args.deployment_name)

if __name__ == "__main__":
    main()

