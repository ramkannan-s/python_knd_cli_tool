# KND Tool
A simple commandline app for kubernetes ngnix deployment

# Installation

## Using Pip (where setup.py file is present)

```bash
  $ pip3 install --editable .
```

## Manual
```bash
  $ git clone https://github.com/ramkannan-s/knd_python_cli_tool
  $ cd knd_python_cli_tool
  $ python3 setup.py install (or) pip3 install --editable .
```

# Usage
```bash
$ knd
```

## RUN

knd -r <replica_count> -v <ngnix_version> -d <nginx_deployment_name>

```bash
$ knd -r 2 -v 1.21 -d nginx-via-knd
```