KND

# NAME
knd

# SYNOPSIS
**knd** [--replicas=replicas] [--nginx-version=version] [deployment-name]

# DESCRIPTION
**knd** (Kubernetes NGINX deployer) deploys NGINX on a Kubernetes cluster, and verifies that it has come up healthy.
A CLI progress bar is provided to indicate the deployment/scaling progress.
The application can be deployed with a configurable number of replicas.
